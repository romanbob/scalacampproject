package edu.bob.scalaCamp.models

case class IotDevice(id: Long, userId: Long, sn: String)