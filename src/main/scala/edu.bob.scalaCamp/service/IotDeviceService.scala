package edu.bob.scalaCamp.service

import cats.Monad
import edu.bob.scalaCamp.models.IotDevice
import edu.bob.scalaCamp.repository.{IotDeviceRepository, UserRepository}

class IotDeviceService[F[_]](repository: IotDeviceRepository[F],
                             userRepository: UserRepository[F])
                            (implicit monad: Monad[F]) {
  import cats.implicits._

  // the register should fail with Left if the user doesn't exist or the sn already exists.
  def registerDevice(userId: Long, sn: String): F[Either[String, IotDevice]] = {

    repository.getBySn(sn).flatMap({
      case Some(iotDevice) =>
        monad.pure(Left(s"Device with number '${iotDevice.sn}' already exists"))
      case None =>
        // .map syntax works because of import cats.implicits._
        // so map function is added to F[_] through implicit conversions
        userRepository.getById(userId).flatMap({
          case Some(iotDevice) =>
            repository.registerDevice(userId, sn).map(Right(_))
          case None =>
            monad.pure(Left(s"User with id $userId does not exist"))
        })
    })
  }
}
