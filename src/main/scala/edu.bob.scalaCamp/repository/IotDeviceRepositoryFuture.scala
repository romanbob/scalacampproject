package edu.bob.scalaCamp.repository

import java.util.concurrent.atomic.AtomicInteger

import edu.bob.scalaCamp.models.IotDevice

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class IotDeviceRepositoryFuture extends IotDeviceRepository[Future] {
  private var counter: AtomicInteger = new AtomicInteger()
  private var storage: Map[String, IotDevice] = Map()

  override def registerDevice(userId: Long, serialNumber: String): Future[IotDevice] = {
    val device = IotDevice(counter.incrementAndGet(), userId, serialNumber)
    storage = storage + (serialNumber -> device)
    Future.successful{ device }
  }

  override def getById(id: Long): Future[Option[IotDevice]] = {
    Future.successful{ storage.values.find(d => d.id.equals(id)) }
  }

  def getBySn(sn: String): Future[Option[IotDevice]] = {
    Future.successful{ storage.get(sn) }
  }

  def getByUser(userId: Long): Future[Seq[IotDevice]] = {
    Future{ storage.values.filter(d => d.id.equals(userId)).toList}
  }
}
