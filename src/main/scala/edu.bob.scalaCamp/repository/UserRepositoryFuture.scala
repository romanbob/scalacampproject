package edu.bob.scalaCamp.repository

import java.util.concurrent.atomic.AtomicInteger

import edu.bob.scalaCamp.models.User

import scala.concurrent.Future

class UserRepositoryFuture extends UserRepository[Future] {
  private var counter: AtomicInteger = new AtomicInteger()
  private var storage: Map[String, User] = Map()

  override def registerUser(username: String, address: String, email: String): Future[User] = {
    val user = User(counter.incrementAndGet(), username, Some(address), email)
    storage = storage + (username -> user)

    Future.successful{user}
  }

  override def getByUsername(username: String): Future[Option[User]] = Future.successful{storage.get(username)}
  override def getById(id: Long): Future[Option[User]] = Future.successful{storage.values.find(u => u.id.equals(id))}
}
