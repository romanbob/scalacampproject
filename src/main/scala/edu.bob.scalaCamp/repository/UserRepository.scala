package edu.bob.scalaCamp.repository

import edu.bob.scalaCamp.models.User

trait UserRepository[F[_]] {
  def registerUser(username: String, address: String, email: String): F[User]
  def getById(id: Long): F[Option[User]]
  def getByUsername(username: String): F[Option[User]]
}
