package edu.bob.scalaCamp.repository

import java.util.concurrent.atomic.AtomicInteger

import cats.Id
import edu.bob.scalaCamp.models.IotDevice

class IotDeviceRepositoryId extends IotDeviceRepository[Id] {
  private var counter: AtomicInteger = new AtomicInteger()
  private var storage: Map[String, IotDevice] = Map()

  override def registerDevice(userId: Long, serialNumber: String): Id[IotDevice] = {
    val device = IotDevice(counter.incrementAndGet(), userId, serialNumber)
    storage = storage + (serialNumber -> device)
    device
  }

  override def getById(id: Long): Id[Option[IotDevice]] = {
    storage.values.find(d => d.id.equals(id))
  }

  def getBySn(sn: String): Id[Option[IotDevice]] = {
    storage.get(sn)
  }

  def getByUser(userId: Long): Id[Seq[IotDevice]] = {
    storage.values.filter(d => d.id.equals(userId)).toList
  }
}
