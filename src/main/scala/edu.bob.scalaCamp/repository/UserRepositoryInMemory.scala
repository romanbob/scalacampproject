package edu.bob.scalaCamp.repository

import java.util.concurrent.atomic.AtomicInteger

import cats.Id
import edu.bob.scalaCamp.models.User

class UserRepositoryInMemory extends UserRepository[Id] {
  private var counter: AtomicInteger = new AtomicInteger()
  private var storage: Map[String, User] = Map()

  override def registerUser(username: String, address: String, email: String): Id[User] = {
    val user = User(counter.incrementAndGet(), username, Some(address), email)
    storage = storage + (username -> user)
    user}

  override def getByUsername(username: String):Id[Option[User]] = {
    storage.get(username)
  }

  override def getById(id: Long):Id[Option[User]] = {
    storage.values.find(u => u.id.equals(id))
  }
}
