package edu.bob.scalaCamp

/**
  * Implement validator typeclass that should validate arbitrary value [T].
  * @tparam T the type of the value to be validated.
  */
trait Validator[T] {
  /**
    * Validates the value.
    * @param value value to be validated.
    * @return Right(value) in case the value is valid, Left(message) on invalid value
    */
  def validate(value: T): Either[String, T]

  /**
    * And combinator.
    * @param other validator to be combined with 'and' with this validator.
    * @return the Right(value) only in case this validator and <code>other</code> validator returns valid value,
    *         otherwise Left with error messages from the validator that failed.
    */
  def and(other: Validator[T]): Validator[T] = {
    val _outer = this
    new Validator[T] {
      override def validate(value: T): Either[String, T] = {
        val leftOperandResult = _outer.validate(value)
        if (leftOperandResult.isLeft)
          leftOperandResult
        else {
          other.validate(value)
        }
      }
    }
  }

  /**
    * Or combinator.
    * @param other validator to be combined with 'or' with this validator.
    * @return the Right(value) only in case either this validator or <code>other</code> validator returns valid value,
    *         otherwise Left with error messages from both validators.
    */
  def or(other: Validator[T]): Validator[T] = {
    val _outer = this
    new Validator[T] {
      override def validate(value: T): Either[String, T] = {
        val leftOperandResult = _outer.validate(value)
        var errorString: String = ""
        if (leftOperandResult.isLeft) {
          errorString += leftOperandResult.left.get
          val rightOperandResult: Either[String, T] = other.validate(value)
          if (rightOperandResult.isLeft) {
            errorString += "; " + rightOperandResult.left.get
            Left(errorString)
          } else {
            rightOperandResult
          }
        } else
          leftOperandResult
      }
    }
  }
}


object Validator {
  val positiveInt: Validator[Int] = new Validator[Int] {
    override def validate(t: Int): Either[String, Int] = {
      if (t >= 0) Right(t)
      else Left("Negative Integer")
    }
  }

  def lessThan(n: Int): Validator[Int] = new Validator[Int] {
    override def validate(t: Int): Either[String, Int] = {
      if (t < n) Right(t)
      else Left(s"Equal or greater than $n")
    }
  }

  val nonEmpty: Validator[String] = new Validator[String] {
    override def validate(t: String): Either[String, String] = {
      if (t.isEmpty) {
        Left("Empty string")
      } else {
        Right(t)
      }
    }
  }

  val isPersonValid: Validator[Person] = new Validator[Person] {
    // Returns valid only when the name is not empty and age is in range [1-99].
    override def validate(value: Person): Either[String, Person] = {
      var errorString: String = ""
      val nameValidator: Either[String, String] = value.name validate nonEmpty
      val ageValidator: Either[String, Int] = value.age validate (positiveInt and lessThan(100))

      if (nameValidator isLeft) {
        errorString = "Property 'name': " + nameValidator.left.get
      }

      if (ageValidator isLeft) {
        if (nameValidator isLeft) {
          errorString += "; "
        }
        errorString += "Property 'age': " + ageValidator.left.get
      }

      if (errorString isEmpty)
        Right(value)
      else
        Left(errorString)
    }
  }

  implicit class IntegerEnricher(i: Int) {
    def validate(implicit validator: Validator[Int]): Either[String, Int] = {
      validator.validate(i)
    }
  }

  implicit class StringEnricher(str: String) {
    def validate(implicit validator: Validator[String]): Either[String, String] = {
      validator.validate(str)
    }
  }

  implicit class PersonEnricher(person: Person) {
    def validate(implicit validator: Validator[Person]): Either[String, Person] = {
      validator.validate(person)
    }
  }

  implicit val defaultIntValidator: Validator[Int] = Validator.positiveInt
  implicit val defaultStringValidator: Validator[String] = Validator.nonEmpty
  implicit val defaultPersonValidator: Validator[Person] = Validator.isPersonValid
}

object ValidApp {
  import Validator._

  // uncomment make possible next code to compile
  2 validate (positiveInt and lessThan(10))

  // uncomment make possible next code to compile
  "" validate Validator.nonEmpty

  // uncomment make possible next code to compile
  Person(name = "John", age = 25) validate isPersonValid
}

object ImplicitValidApp {
  import Validator._

  Person(name = "John", age = 25) validate;
  "asdasd" validate;
  234.validate
}

case class Person(name: String, age: Int)