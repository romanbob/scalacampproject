import edu.bob.scalaCamp.models.IotDevice
import edu.bob.scalaCamp.repository.{IotDeviceRepositoryId, UserRepositoryInMemory}
import edu.bob.scalaCamp.service.{IotDeviceService, UserService}
import org.scalatest.FunSuite

class IotDeviceServiceTest extends FunSuite {

  test("IotDeviceService test") {
    val inMemoryDeviceRepository = new IotDeviceRepositoryId
    val inMemoryUserRepository = new UserRepositoryInMemory
    val userService = new UserService(inMemoryUserRepository)

    val user1 = userService.registerUser("Roman", "Lviv", "roman@google.com")
    val user2 = userService.registerUser("Vasyl", "Kyiv", "vasyl@google.com")
    val user3 = userService.registerUser("Yuriy", "Toronto", "yuriy@google.com")

    val iotDeviceService = new IotDeviceService(inMemoryDeviceRepository, inMemoryUserRepository)
    assert(iotDeviceService.registerDevice(4, "number").equals(Left("User with id 4 does not exist")))
    assert(iotDeviceService.registerDevice(3, "number").equals(Right(IotDevice(1,3,"number"))))
    assert(iotDeviceService.registerDevice(3, "number").equals(Left("Device with number 'number' already exists")))
  }
}
