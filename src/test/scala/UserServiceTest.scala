import cats.instances.future._
import edu.bob.scalaCamp.models.User
import edu.bob.scalaCamp.repository.{UserRepositoryFuture, UserRepositoryInMemory}
import edu.bob.scalaCamp.service.UserService
import org.scalatest.FunSuite

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.higherKinds

class UserServiceTest extends FunSuite {

  test("UserService with IdRepository test") {
    val inMemoryRepository = new UserRepositoryInMemory
    val userService = new UserService(inMemoryRepository)

    val user1 = userService.registerUser("Roman", "Lviv", "roman@google.com").right.get
    val user2 = userService.registerUser("Vasyl", "Kyiv", "vasyl@google.com").right.get
    val user3 = userService.registerUser("Yuriy", "Toronto", "yuriy@google.com").right.get

    assert(user3.equals(userService.getById(user3.id).get))

    userService.getByUsername(user2.username)
    assert(user2.equals(userService.getByUsername(user2.username).get))
  }

  test("UserService with FutureRepository test") {
    val futureRepository = new UserRepositoryFuture
    val userService = new UserService(futureRepository)

    def await[T](f: Future[T]) = Await.result(f, 2.seconds)

    await(userService.registerUser("Roman", "Lviv", "roman@google.com"))
    await(userService.registerUser("Vasyl", "Kyiv", "vasyl@google.com"))
    await(userService.registerUser("Yuriy", "Toronto", "yuriy@google.com"))

    assert(await(userService.getById(3)).contains(User(3, "Yuriy", Some("Toronto"), "yuriy@google.com")))
    assert(await(userService.getByUsername("Roman")).contains(User(1, "Roman", Some("Lviv"), "roman@google.com")))
  }
}
