import edu.bob.scalaCamp.Person
import edu.bob.scalaCamp.Validator._
import org.scalatest.FunSuite

class ValidatorTest extends FunSuite {

  test("Integer Validator. Check if positive") {
    val positiveIntValidator =  positiveInt
    assert(positiveIntValidator.validate(-1) == Left("Negative Integer"))
    assert(positiveIntValidator.validate(3) == Right(3))
  }

  test("Integer Validator. Check if less than 'n'") {
    val lessThanIntValidator = lessThan(100)
    assert(lessThanIntValidator.validate(101) == Left("Equal or greater than 100"))
    assert(lessThanIntValidator.validate(99) == Right(99))
  }

  test("isPersonValid") {
    assert((isPersonValid validate Person("Bob", 25)) == Right(Person("Bob", 25)))
    assert((isPersonValid validate Person("", 25)) == Left("Property 'name': Empty string"))
    assert((isPersonValid validate Person("Bob", 101)) == Left("Property 'age': Equal or greater than 100"))
    assert((isPersonValid validate Person("", -101)) == Left("Property 'name': Empty string; Property 'age': Negative Integer"))
  }

  test("And combinator") {
    assert((positiveInt and lessThan(-100)).validate(-10) == Left("Negative Integer"))
    assert((lessThan(-100) and positiveInt).validate(-10) == Left("Equal or greater than -100"))
    assert((lessThan(-100) and positiveInt and positiveInt).validate(-10) == Left("Equal or greater than -100"))
  }

  test("Or combinator") {
    assert((lessThan(-50) or positiveInt).validate(-1) == Left("Equal or greater than -50; Negative Integer"))
    assert(((lessThan(-50) or positiveInt).validate(-51)) == Right(-51))
    assert(((positiveInt or lessThan(-100)).validate(-101)) == Right(-101))
    assert(((lessThan(10) or positiveInt).validate(5)) == Right(5))
    assert(((lessThan(-50) or positiveInt or positiveInt).validate(-1)) == Left("Equal or greater than -50; Negative Integer; Negative Integer"))
  }

  test("Implicit class") {
    assert((20 validate(positiveInt and lessThan(10))) == Left("Equal or greater than 10"))
    assert(("" validate nonEmpty) == Left("Empty string"))
  }

  test( "Implicit validator") {
    assert((Person("", -10) validate) == Left("Property 'name': Empty string; Property 'age': Negative Integer"))

    assert(("" validate) == Left("Empty string"))

    assert((-1 validate) == Left("Negative Integer"))
  }
}
