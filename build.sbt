name := "scalacamp"

version := "0.1"

scalaVersion := "2.11.8"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test
libraryDependencies += "org.typelevel" %% "cats-core" % "1.6.0-RC1"